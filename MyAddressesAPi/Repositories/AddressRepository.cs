﻿using Microsoft.EntityFrameworkCore;
using MyAddressesAPi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyAddressesAPi.Repositories
{
    public class AddressRepository : IAddressRepository
    {
        private readonly AddressContext _context;
        public AddressRepository(AddressContext context)
        {
            _context = context;

        }
        public async Task<Address> Create(Address address)
        {
            _context.Addresses.Add(address);
            await _context.SaveChangesAsync();

            return address;

        }

        public async Task Delete(int Id)
        {
            var addressToDelete = await _context.Addresses.FindAsync(Id);
            _context.Addresses.Remove(addressToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Address>> GetAll()
        {

            return await _context.Addresses.ToListAsync();
        }

        public async Task<Address> Get(int Id)
        {
            return await _context.Addresses.FindAsync(Id);
        }
         public async Task <IEnumerable<Address>>GetAll([FromQuery] string searchterm, string orderList)
        { 
            if (string.IsNullOrWhiteSpace(searchterm))
                return await GetAll();
            var collection = _context.Addresses as IQueryable<Address>;
            searchterm = searchterm.Trim();
           collection = collection.Where(a => a.Country == searchterm.ToUpper() || a.Number==searchterm || a.Place.Contains(searchterm) || a.Street.Contains(searchterm) || a.PostalCode == searchterm).OrderBy(a => a.Street).ThenBy(a=>a.Country);
            
            return  collection;
        }

     

        public async Task Update(Address address)
        {
            _context.Entry(address).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
