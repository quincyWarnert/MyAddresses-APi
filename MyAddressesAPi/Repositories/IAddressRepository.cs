﻿using MyAddressesAPi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAddressesAPi.Repositories
{
   public  interface IAddressRepository
    {
        Task<IEnumerable<Address>> GetAll();
        Task<IEnumerable<Address>> GetAll(string searchTerm);
      
        Task<Address> Get(int Id);
        Task<Address> Create(Address address);
        Task Update(Address address);
        Task Delete(int Id);
    }
}
