﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyAddressesAPi.Models;
using MyAddressesAPi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAddressesAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly IAddressRepository _addressRepository;
        public AddressController(IAddressRepository addressRepository){
            _addressRepository = addressRepository;
        }
        [HttpGet]
        public async Task<IEnumerable<Address>> GetAddresses([FromQuery]string searchterm)
        {
            return await _addressRepository.GetAll(searchterm);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Address>>GetAddress(int id)
        {
            return await _addressRepository.Get(id);
        }

      
        [HttpPost]
        public async Task<ActionResult<Address>>PostAddress([FromBody]Address address)
        {
            var newAddress = await _addressRepository.Create(address);
            return CreatedAtAction(nameof(GetAddresses), new { id = newAddress.Id }, newAddress);
        }
        [HttpPut]
        public async Task<ActionResult> PutAddress(int id,[FromBody] Address address)
        {
            if(id!=address.Id)
            {
                return BadRequest();
            }
            await _addressRepository.Update(address);
            return NoContent();
        }
        [HttpDelete]

        public async Task<ActionResult>Delete(int id)
        {

            var addressToDelete = await _addressRepository.Get(id);
            if(addressToDelete == null)            
                return NotFound();
            
            await _addressRepository.Delete(addressToDelete.Id);

            return NoContent();
        }
    }
}
