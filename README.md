**Use DEVELOPER BRANCH merge into master has some conflicts**

**MyAddressesAPi**

ASP.Net web Api

**SDK**
- .NET 5


**Packages**
- Microsoft.EntityFrameworkCore.Sqlite(5.0.9)
- Swashbuckle.AspNertCore(6.2.1)
- Geolocation(1.2.1) *

**Usage**
This web api application consists of basic crud endpoints that call on an Sqlite Database
with an Address model. (mytestaddress.db located in the root)

**Database**
The data base consists of string attributes and an int ID. I would expand this by adding constraints such as NOT NULL and Primaire key.

- The Create (POST) endpoint takes a Json build of the model for data entry
{
  "id": 0,
  "street": "string",
  "number": "string",
  "postalCode": "string",
  "place": "string",
  "country": "string"
}


- The Read (GET) endoint has A getAll, get by Id and a search option. 
The search covers all the fields.
I would have liked to refine the search it a little better so as it could work with case sensitve text entry.
Adding a to lowercase before the data is commited to the database for example. 
Also the sorting is done on the get request. it sorts by street and country.


- The geoLocation I didnt get a chance to implement that feature yet.
Bu the idea was to calculate the distance between two given points using the long and lat.

For this feature I was going to use a view, ViewModel, Model and a Controller.
Download the geolocation package. so that I could make use of the various methods to calculate the distances
based of off the entered long and lat fields. The fields would have been added to the existing  address model.
Using the google api to find the correct coordinates once the address is entered into the database. 





